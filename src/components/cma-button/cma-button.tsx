import { Component, h, Host,Prop } from '@stencil/core';

@Component({
  tag: 'cma-button',
  styleUrl: 'cma-button.less',
  shadow: false,
})
export class CmaButton {

  @Prop() class: string;
  @Prop() props: object;
  // @Prop() id: string;
  @Prop() disabled: boolean;



  render() {
    return (
      <Host aria-hidden={'true'}  {...this.props} v-cloak>
          <slot></slot>
      </Host>
    );
  }

}
