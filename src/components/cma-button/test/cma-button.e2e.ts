import { newE2EPage } from '@stencil/core/testing';

describe('cma-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cma-button></cma-button>');

    const element = await page.find('cma-button');
    expect(element).toHaveClass('hydrated');
  });
});
