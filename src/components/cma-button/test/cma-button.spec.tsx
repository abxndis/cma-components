import { newSpecPage } from '@stencil/core/testing';
import { CmaButton } from '../cma-button';

describe('cma-button', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CmaButton],
      html: `<cma-button></cma-button>`,
    });
    expect(page.root).toEqualHtml(`
      <cma-button>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </cma-button>
    `);
  });
});
