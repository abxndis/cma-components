import { html } from 'lit-html';

export default {
  title: 'Components/Buttons',
  component: 'my-component', // which is also found in the `custom-elements.json`
  argTypes: {
    first: { control: { type: 'text' } },
    middle: { control: { type: 'text' } },
    last: { control: { type: 'text' } },
    className: { control: { type: 'text' } },
  }
};

export const basic = ({ first, middle, last, className }) => html`
<cma-button class="${className}"> ${first} ${middle}</cma-button>

<cma-button class="${className}" disabled="true">  ${first} ${middle}</cma-button>

<cma-button class="${className}" id="MONGOSTO" > ${first} ${middle}</cma-button>
`;
basic.args = { 
    first: "Primary", 
    middle: "😎",
    last: "'Don't call me a framework' JS",
    className:'btn-primary'
};