# cma-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type      | Default     |
| ---------- | ---------- | ----------- | --------- | ----------- |
| `class`    | `class`    |             | `string`  | `undefined` |
| `disabled` | `disabled` |             | `boolean` | `undefined` |
| `props`    | --         |             | `object`  | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
