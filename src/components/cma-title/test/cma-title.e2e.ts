import { newE2EPage } from '@stencil/core/testing';

describe('cma-title', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cma-title></cma-title>');

    const element = await page.find('cma-title');
    expect(element).toHaveClass('hydrated');
  });
});
