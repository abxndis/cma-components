import { newSpecPage } from '@stencil/core/testing';
import { CmaTitle } from '../cma-title';

describe('cma-title', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CmaTitle],
      html: `<cma-title></cma-title>`,
    });
    expect(page.root).toEqualHtml(`
      <cma-title>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </cma-title>
    `);
  });
});
