import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'cma-title',
  styleUrl: 'cma-title.less',
  shadow: true,
})
export class CmaTitle {

  render() {
    return (
      <Host>
       <h1>
          <slot></slot>
       </h1>
      </Host>
    );
  }

}
