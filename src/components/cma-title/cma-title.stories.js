import { html } from 'lit-html';

export default {
  title: 'Components/Title',
  component: 'my-component', // which is also found in the `custom-elements.json`
  argTypes: {
    first: { control: { type: 'text' } },
    middle: { control: { type: 'text' } },
    last: { control: { type: 'text' } },
  }
};

export const basic = ({ first, middle, last }) => html`
<cma-title>${last}</cma-title>
`;
basic.args = { 
    first: "Primary", 
    middle: "😎",   
    last: "'Don't call me a framework' JS",
    class:'btn-primary'
};